(function () {

    angular.module('layoutModule').controller('MenuController', ['$scope','MenuService', function ($scope,MenuService) {

            MenuService.getMenuLeft().success(function (data)
            {
                $scope.menusLeft = data;
            });

            MenuService.getMenuRight().success(function (data)
            {
                $scope.menusRight = data;
            });

            MenuService.getMenuRightConnected().success(function (data)
            {
                $scope.menusRightConnected = data;
            });

        }]);

})();