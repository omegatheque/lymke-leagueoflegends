(function(){
    
    angular.module('userModule').config(['$locationProvider', '$routeProvider',
            function($location, $routeProvider) {
              $routeProvider.

                when('/register', {
                  templateUrl: 'modules/user/views/register.html',
                  controller: 'RegisterController',
                  access: { requiredLogin: false,requireNotLogin:true },
                  themepage : "caitlyn"
                }).
                        
                when('/login', {
                  templateUrl: 'modules/user/views/login.html',
                  controller: 'LoginController',
                  access: { requiredLogin: false,requireNotLogin:true },
                  themepage : "caitlyn"
                }).
                when('/logout', {
                  templateUrl: 'modules/user/views/logout.html',
                  controller: 'LogoutController',
                  access: { requiredLogin: false,requireNotLogin:false }
                }).
                when('/valideremail/:tocken', {
                  templateUrl: 'modules/user/views/valideremail.html',
                  controller: 'ValiderEmailController',
                  access: { requiredLogin: false,requireNotLogin:false }
                }).
                when('/profil', {
                  templateUrl: 'modules/user/views/profil.html',
                  controller: 'ProfilController',
                  access: { requiredLogin: true,requireNotLogin:false },
                  themepage : "profil"
                });
                
            }]);
})();