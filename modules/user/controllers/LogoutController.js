(function(){
    
    angular.module('userModule').controller('LogoutController',['$scope','$http','vcRecaptchaService','$location','UserService','$localStorage',
                                                     function($scope,$http,vcRecaptchaService,$location,UserService,$localStorage){
            
        delete $localStorage.token;
        delete $localStorage.user;       
       
    }]);
    
})();