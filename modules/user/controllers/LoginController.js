(function () {

    angular.module('userModule').controller('LoginController', ['$scope', '$location', 'UserService', '$localStorage',
        function ($scope, $location, UserService, $localStorage) {


            $scope.logIn = function () {

                UserService.logIn($scope.user).then(function (response)
                {
                    if(response.bSuccess == true)
                    {   $scope.isLogged = true;
                        $localStorage.token = response.oJWT;
                        $localStorage.user = response.oUser;
                        $localStorage.isNewUser = false;
                        $localStorage.isUnAuthorizedUser = false;
                        $location.path('/profil');
                    }
                    else
                    {   $scope.errorLogin = true;
                        console.log('Une erreur s\'est glissée dans le formulaire.');
                    }
                });
            };


        }]);

})();