(function(){
    
    angular.module('userModule').controller('ValiderEmailController',['$scope','$http','$location','UserService','$localStorage','$routeParams',
                                                     function($scope,$http,$location,UserService,$localStorage,$routeParams){
            

        UserService.validerMail($routeParams.tocken)
                    .success(function(response){
                        if(response.success){
                            $scope.successMail = response.success;
                            if(UserService.isLogged()){
                               $localStorage.user.email_validated = 1;
                            }
                        }
                    })
                    .error(function(response){
                        $scope.successMail = false;
                        console.log('Une erreur a eu lieu :/');
                    });
       
    }]);
    
})();