(function () {
    angular.module('userModule').directive('facebook', function(){
         
        return {
            restrict: 'E',
            templateUrl: angular.module('userModule').path + 'templates/facebook.html',
            link : function(){
            }
        };
    });
})();