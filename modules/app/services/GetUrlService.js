(function(){
    angular.module('defaultApp').factory('GetUrlService',function($http,$q,$localStorage) 
    {
            return{
                getUrlVideoYoutube : function (videoId) 
                {   
                    return 'https://www.youtube.com/embed/' + videoId + '?rel=0';
                }
            };
    });    

})();