(function(){

    angular.module('defaultApp').config(['$locationProvider', '$routeProvider',
            function($location, $routeProvider) {
              $routeProvider.
                
            //Default
    
                when('/accueil', {
                  templateUrl: 'modules/app/views/accueil.html',
                  controller: '',
                  access: { requiredLogin: false },
                  themepage : "tristana"
                }).
                        
            //Otherwise

                otherwise({
                  redirectTo: '/accueil',
                  access: { requiredLogin: false }
                });
            }]);
})();