(function () {
    angular.module('defaultApp').run(['$rootScope', '$location', '$localStorage','UserService', 'GetUrlService', 'TraductionService',
        function ($rootScope, $location, $localStorage, UserService, GetUrlService, TraductionService) {
            $rootScope.$on('$routeChangeStart', function (event, nextRoute, currentRoute) {
                $rootScope.location = $location;
                $rootScope.TraductionService = TraductionService;
                $rootScope.GetUrlService = GetUrlService;
                $rootScope.UserService = UserService;
                
                $rootScope.themepage = "";
                if(nextRoute.themepage != undefined){
                    $rootScope.themepage = "theme-"+nextRoute.themepage;
                }

            });
        }]);
})();