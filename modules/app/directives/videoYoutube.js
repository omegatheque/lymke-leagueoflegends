(function () {
    angular.module('defaultApp').directive('videoYoutube', [function(GetUrl){
         
        return {
            restrict: 'E',
            templateUrl: angular.module('defaultApp').path + 'templates/video-youtube.html',
            scope : {
               widthYoutube : '@',
               heightYoutube : '@',
               urlYoutube : '@'
            }
        };
    }]);
})();