(function(){
    
    angular.module('toolModule')
           .controller('creepController',
            ['$scope','$routeParams','$http','$location','$localStorage',
    function($scope,$routeParams, $http,$location,$localStorage)
    {
	   $scope.sbireMeleGold = 20;
	   $scope.sbireDistanceGold = 15;
	   $scope.sbireCanonGold = 41;
	   
	   $scope.stats = {
                            "totalGolds" : 0,
                            "totalCreeps" : 0,
                            "totalGoldsLastHit" : 0,
                            "totalCreepsLastHit" : 0
			  };
	   
       $scope.vagues = [
                        {
                           "numero":1,
                           "minute":"1:29",
                           "sbireMele":3,
                           "sbireCanon":0,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":2,
                           "minute":"1:59",
                           "sbireMele":3,
                           "sbireCanon":0,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":3,
                           "minute":"2:29",
                           "sbireMele":3,
                           "sbireCanon":1,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":5,
                           "minute":"2:59",
                           "sbireMele":3,
                           "sbireCanon":0,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":4,
                           "minute":"3:29",
                           "sbireMele":3,
                           "sbireCanon":0,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":6,
                           "minute":"3:59",
                           "sbireMele":3,
                           "sbireCanon":1,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":7,
                           "minute":"4:29",
                           "sbireMele":3,
                           "sbireCanon":0,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":8,
                           "minute":"4:59",
                           "sbireMele":3,
                           "sbireCanon":0,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":9,
                           "minute":"5:29",
                           "sbireMele":3,
                           "sbireCanon":1,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":10,
                           "minute":"5:59",
                           "sbireMele":3,
                           "sbireCanon":0,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        },
                        {
                           "numero":11,
                           "minute":"6:29",
                           "sbireMele":3,
                           "sbireCanon":0,
                           "sbireDistance":3,
                           "sbireMeleLastHit":0,
                           "sbireCanonLastHit":0,
                           "sbireDistanceLastHit":0
                        }
                     ];
                     $scope.initTotal = function(){
                        angular.forEach($scope.vagues, function(vague, key) {
                            console.log(vague);
                                  
                                $scope.stats.totalGolds += vague.sbireMele *  $scope.sbireMeleGold;
                                $scope.stats.totalGolds += vague.sbireDistance *  $scope.sbireDistanceGold;
                                $scope.stats.totalGolds += vague.sbireCanon *  $scope.sbireCanonGold;
                                  
                                $scope.stats.totalCreeps += vague.sbireMele;
                                $scope.stats.totalCreeps += vague.sbireDistance;
                                $scope.stats.totalCreeps += vague.sbireCanon;
                                  
                                
                        });
                    };
                        
		$scope.calculStats = function(){
                        $scope.stats.totalGoldsLastHit = 0;
                        $scope.stats.totalCreepsLastHit = 0;
			angular.forEach($scope.vagues, function(vague, key) {
                            $scope.stats.totalGoldsLastHit += vague.sbireMeleLastHit *  $scope.sbireMeleGold;
                            $scope.stats.totalGoldsLastHit += vague.sbireDistanceLastHit *  $scope.sbireDistanceGold;
                            $scope.stats.totalGoldsLastHit += vague.sbireCanonLastHit *  $scope.sbireCanonGold;
                                  
                            $scope.stats.totalCreepsLastHit += vague.sbireMeleLastHit;
                            $scope.stats.totalCreepsLastHit += vague.sbireDistanceLastHit;
                            $scope.stats.totalCreepsLastHit += vague.sbireCanonLastHit;
                    });
		};
		
    }]);
    
})();