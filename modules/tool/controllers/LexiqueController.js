(function(){
    
    angular.module('toolModule')
           .controller('lexiqueController',
            ['$scope','$routeParams','$http','$location','$localStorage','LexiqueService',
    function($scope,$routeParams, $http,$location,$localStorage,LexiqueService)
    {

	    LexiqueService.getLexique().success(function (data)
        {  
            $scope.words = data;
        });
		
    }]);
    
})();