(function(){
angular.module('toolModule').factory('LexiqueService',['$http','$q','$localStorage',function($http,$q,$localStorage) {
        return{

            getLexique : function ()
            {                
                //var url = angular.module('toolModule').path + "data/lexique/"+$localStorage.lang+".json";
                var url = angular.module('toolModule').path + "data/lexique/lexique.json";
                return $http.get(url);
            }
        };
}]);    

})();