(function(){

    angular.module('toolModule').config(['$locationProvider', '$routeProvider',
            function($location, $routeProvider) {
              $routeProvider.
                
                when('/creep', {
                  templateUrl: angular.module('toolModule').path + 'views/creep.html',
                  controller: 'creepController',
                  access: { requiredLogin: false }
                }).when('/lexique', {
                  templateUrl: angular.module('toolModule').path + 'views/lexique.html',
                  controller: 'lexiqueController',
                  access: { requiredLogin: false }
                });

            }]);
})();