(function () {
    angular.module('championModule').directive('championMenu', function(){
         
        return {
            restrict: 'EA',
            scope : {
                champion : '=',
                itemSelected : '@'
            },
            templateUrl: angular.module('championModule').path + 'templates/champion-menu.html',
            link : function($scope, $element){
            }
        };
    });
})();