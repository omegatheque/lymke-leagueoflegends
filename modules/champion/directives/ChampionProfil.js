(function () {
    angular.module('championModule').directive('championProfil', function(){
         
        return {
            restrict: 'E',
            templateUrl: angular.module('championModule').path + 'templates/champion-profil.html'
        };
    });
})();