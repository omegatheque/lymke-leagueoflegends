(function () {
    angular.module('championModule').directive('championList',['$location', function($location){
         
        return {
            restrict: 'E',
            scope : {
                champions : '=',
                version : '=',
                nbperrow : '=',
                query : '=',
                showprix : '='
            },
            templateUrl: angular.module('championModule').path + 'templates/champion-list.html',
            link : function($scope, $element){
                $scope.selectChampion = function(id){
                    $location.path( "/champions/" + id);
                };
                $scope.rangeNbChamp = [];
                for(i=0;i<$scope.nbperrow;i++){
                    $scope.rangeNbChamp.push(i);
                }
            }
        };
    }]);
})();