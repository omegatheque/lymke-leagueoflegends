(function () {
    angular.module('championModule').directive('championSkin', function(){
         
        return {
            restrict: 'E',
            templateUrl: angular.module('championModule').path + 'templates/champion-skin.html'
        };
    });
})();