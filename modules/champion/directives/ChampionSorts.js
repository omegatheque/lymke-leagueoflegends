(function () {
    angular.module('championModule').directive('championSorts', function(){
         
        return {
            restrict: 'E',
            templateUrl: angular.module('championModule').path + 'templates/champion-sorts.html'
        };
    });
})();