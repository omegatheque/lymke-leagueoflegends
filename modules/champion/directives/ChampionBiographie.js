(function () {
    angular.module('championModule').directive('championBiographie', function(){
         
        return {
            restrict: 'E',
            templateUrl: angular.module('championModule').path + 'templates/champion-biographie.html'
        };
    });
})();