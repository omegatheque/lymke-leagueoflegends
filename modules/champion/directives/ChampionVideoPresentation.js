(function () {
    angular.module('championModule').directive('championVideoPresentation', ['GetUrl',function(GetUrl){
         
        return {
            restrict: 'E',
            templateUrl: angular.module('championModule').path + 'templates/champion-video-presentation.html',
            link : function($scope, $element){
                $scope.GetUrl = GetUrl;
                //console.log(scope.videos.presentation);
            }
        };
    }]);
})();
