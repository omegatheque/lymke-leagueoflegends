(function(){
angular.module('championModule').factory('ChampionService',['$http','$q','$localStorage',function($http,$q,$localStorage) {
        return{
            
            /*getChampions : function ()
            {                
                var url = angular.module('championModule').path + "data/champions.json";
                //var url = appConfig.serverRest + "get-champions";
                return $http.get(url);
            },*/
            getChampion : function (id)
            {                
                //var url = appConfig.serverRest + "get-champion/id_champion/"+id+"?lang="+$localStorage.lang;
                var url = angular.module('championModule').path + "data/"+$localStorage.lang+"/"+id+".json";
                return $http.get(url);
            },
            getVideos : function ()
            {                
                var url = angular.module('championModule').path + "data/champion_videos.json";
                return $http.get(url);
            },
            setVideos : function (skin)
            {                
                var deferred = $q.defer();
                var url = appConfig.server + "lol/skin/set-video";
                var datasPost = skin;
                var serviceConf = appConfig.serviceConf;
                
                $http.post(url, datasPost, serviceConf).success(function (response)
                {   
                    deferred.resolve(response);
                }).error(function (response) {
                    console.log(response);
                    deferred.reject(response);
                });
                return deferred.promise;
            },
            getChampions : function ()
            {   var deferred = $q.defer(); 

                console.log(Math.floor(Date.now() / 1000) - $localStorage.champions_timestamp);
                if(     angular.isDefined($localStorage.champions_timestamp) && angular.isDefined($localStorage.champions) && angular.isDefined($localStorage.version) 
                    && (Math.floor(Date.now() / 1000) - $localStorage.champions_timestamp) < appConfig.expirationTime)
                {
                    response = {
                        data : [],
                        version : ''
                    };
                    response.data = $localStorage.champions;
                    response.version = $localStorage.version;
                    deferred.resolve(response);
                }else{
                    var url = angular.module('championModule').path + "data/champions.json";
                    //var url = appConfig.serverRest + "get-champions?lang="+$localStorage.lang";
                    $http.get(url).success(function(response){
                                    $localStorage.champions_timestamp = Math.floor(Date.now() / 1000);
                                    deferred.resolve(response);
                                }).error(function(response){
                                        deferred.reject(response);
                                }
                    );
                }
                return deferred.promise;
            }
            
            /**
             * this.getById = function (datasPost)
            {
                var deferred = $q.defer();

                var url = "/zs_datas/articles/crud";
                $http.post(url, datasPost, serviceConf).success(function (response)
                {
                    deferred.resolve(response);
                }).error(function (response) {
                    deferred.reject(response);
                });
                return deferred.promise;
            };
             */
        };
}]);    

})();