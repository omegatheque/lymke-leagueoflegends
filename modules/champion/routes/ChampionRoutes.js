(function(){

    angular.module('championModule').config(['$locationProvider', '$routeProvider',
            function($location, $routeProvider) {
              $routeProvider.
                
                when('/champions', {
                  templateUrl: angular.module('championModule').path + 'views/champions.html',
                  controller: 'championsController',
                  access: { requiredLogin: false }
                }).
                when('/champions/stats', {
                  templateUrl: angular.module('championModule').path + 'views/champions-stats.html',
                  controller: 'championsStatsController',
                  access: { requiredLogin: false }
                }).
                when('/champions/:id', {
                  templateUrl: angular.module('championModule').path + 'views/champion.html',
                  controller: 'championController',
                  access: { requiredLogin: false }
                }).
                when('/champions/:id/stats', {
                  templateUrl: angular.module('championModule').path + 'views/champion-stats.html',
                  controller: 'championStatsController',
                  access: { requiredLogin: false }
                }).
                when('/champions/:id/skins', {
                  templateUrl: angular.module('championModule').path + 'views/champion-skins.html',
                  controller: 'championSkinsController',
                  access: { requiredLogin: false }
                });

            }]);
})();