(function(){
    
    angular.module('championModule')
           .controller('championsStatsController',
            ['GetUrlService','$scope','$routeParams','$http','$location','$localStorage','ChampionService','ChartJs',
    function(GetUrlService,$scope,$routeParams, $http,$location,$localStorage,ChampionService,ChartJs)
    {
        $scope.videos = [];
        $scope.GetUrlService = GetUrlService;
        ChampionService.getChampion($routeParams.id)
                       .success(function(response){
                            $scope.champion = response;
                        })
                       .error(function(response){
                                    alert('Une erreur a eu lieu :/');
                        });

         if(angular.isDefined($localStorage.champions) && angular.isDefined($localStorage.version) ){
                $scope.champions = $localStorage.champions;
                $scope.version = $localStorage.version;
            }else{
                ChampionService.getChampions()
                               .success(function(response){
                                    $localStorage.champions = response.data;
                                    $localStorage.version = response.version;
                                    $scope.champions = response.data;
                                    $scope.version = response.version;
                                })
                                .error(function(response){
                                    alert('Une erreur a eu lieu :/');
                                }
                );
            }
            
            
            
       /*******************************************************/
       
       $scope.optionsChart = {
                //animation:false
            };

            // ==========================================
            $scope.vente_mensu = {
                labels: [
                    "OR",
                    "ARGENT",
                    "BRONZE",
                    "PLATINE",
                    "CHALLENGER"
                ],
                data: [
                    30,
                    130,
                    101,
                    54,
                    22
                ]
            };
            
            // ===========================================
            $scope.vente_annu = {
                labels : ['janv', 'fév.', 'mars', 'mai', 'juin', 'juil.', 'août', 'sept.', 'oct.', 'nov.', 'déc.'],
                series : ['OR', 'ARGENT', 'BRONZE', 'PLATINE'],
                datas_2015: [
                    [12, 15, 11, 13, 18, 20], // 
                    [22, 25, 21, 23, 28, 30],  //
                    [8, 4, 5, 9, 10, 3],  // 
                    [2, 3, 6, 0, 2, 3]  //
                ],
                datas_2014: [
                    [14, 17, 12, 11, 14, 14, 12, 16, 18, 10, 22, 24], // 
                    [20, 21, 30, 23, 21, 24, 12, 10, 25, 30, 34, 54],  //
                    [8, 4, 5, 9, 10, 3, 4, 2, 1, 12, 13, 20],  // 
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]  // 
                ]
                
            };
            
    }]);
    
})();