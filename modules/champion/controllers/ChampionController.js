(function(){
    
    angular.module('championModule')
           .controller('championController',
            ['GetUrlService','$scope','$routeParams','$http','$location','$localStorage','ChampionService',
    function(GetUrlService,$scope,$routeParams, $http,$location,$localStorage,ChampionService)
    {
        $scope.videos = [];
        $scope.GetUrlService = GetUrlService;
        ChampionService.getChampion($routeParams.id)
                       .success(function(response){
                            $scope.champion = response;
                        })
                       .error(function(response){
                                    alert('Une erreur a eu lieu :/');
                        });
                             
        ChampionService.getVideos()
                       .success(function(response){
                           
                            $scope.videos = response[$routeParams.id];

                            $scope.auMoinsUneVideo = false;
                            taille = $scope.videos.list.length;
                            for (var i = 0; i < taille; i++) {
                                if(!$scope.auMoinsUneVideo  && $scope.videos.list[i].id !== ''){
                                    $scope.videos.list[i].show = true;
                                    $scope.auMoinsUneVideo = true;
                                }
                                $scope.videos.list[i].url = GetUrlService.getUrlVideoYoutube($scope.videos.list[i].id);
                              }
                            }
                            
                    
                       )
                       .error(function(response){
                                    alert('Une erreur a eu lieu :/');
                        });
        
        $scope.toggleVideo = function(v){
                taille = $scope.videos.list.length;
                for (var i = 0; i < taille; i++) {
                   $scope.videos.list[i].show = false;
                }

                v.show = true;  
         };
         
         
         if(angular.isDefined($localStorage.champions) && angular.isDefined($localStorage.version) ){
                $scope.champions = $localStorage.champions;
                $scope.version = $localStorage.version;
            }else{
                ChampionService.getChampions()
                               .success(function(response){
                                    $localStorage.champions = response.data;
                                    $localStorage.version = response.version;
                                    $scope.champions = response.data;
                                    $scope.version = response.version;
                                })
                                .error(function(response){
                                    alert('Une erreur a eu lieu :/');
                                }
                );
            }
            
    }]);
    
})();