(function(){
    
    angular.module('championModule')
           .controller('championSkinsController',
            ['GetUrlService','$scope','$routeParams','$http','$location','$localStorage','ChampionService','UserService','ChartJs',
    function(GetUrlService,$scope,$routeParams, $http,$location,$localStorage,ChampionService,UserService,ChartJs)
    {
        $scope.videos = [];
        $scope.GetUrlService = GetUrlService;
        $scope.skinSelected = {};
        $scope.UserService = UserService;
        ChampionService.getChampion($routeParams.id)
                       .success(function(response){
                            $scope.champion = response;
                            
                        })
                       .error(function(response){
                                    alert('Une erreur a eu lieu :/');
                        });

         if(angular.isDefined($localStorage.champions) && angular.isDefined($localStorage.version) ){
                $scope.champions = $localStorage.champions;
                $scope.version = $localStorage.version;
            }else{
                ChampionService.getChampions()
                               .success(function(response){
                                    $localStorage.champions = response.data;
                                    $localStorage.version = response.version;
                                    $scope.champions = response.data;
                                    $scope.version = response.version;
                                    
                                })
                                .error(function(response){
                                    alert('Une erreur a eu lieu :/');
                                }
                );
            }
            
        

        $scope.setSelected = function(skin){
            $scope.skinSelected = skin;
        };
        
       $scope.validateSkin = function(skin){
           skin.alert = {};
           ChampionService.setVideos(skin).then(function (response)
                    {  
                        if(response.bSuccess){
                            skin.alert = {};
                            skin.alert.type = 'success';
                        }else{
                            
                        }
                    });
           
       };
       $scope.closeAlertSkin = function(skin){
           skin.alert = false;
       };
       
        
    }]);
    
})();