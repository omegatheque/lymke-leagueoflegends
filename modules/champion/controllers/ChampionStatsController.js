(function(){
    
    angular.module('championModule')
           .controller('championStatsController',
            ['GetUrlService','$scope','$routeParams','$http','$location','$localStorage','ChampionService','ChartJs',
    function(GetUrlService,$scope,$routeParams, $http,$location,$localStorage,ChampionService,ChartJs)
    {
        $scope.videos = [];
        $scope.GetUrlService = GetUrlService;
        ChampionService.getChampion($routeParams.id)
                       .success(function(response){
                            $scope.champion = response;
                        })
                       .error(function(response){
                                    alert('Une erreur a eu lieu :/');
                        });

         if(angular.isDefined($localStorage.champions) && angular.isDefined($localStorage.version) ){
                $scope.champions = $localStorage.champions;
                $scope.version = $localStorage.version;
            }else{
                ChampionService.getChampions()
                               .success(function(response){
                                    $localStorage.champions = response.data;
                                    $localStorage.version = response.version;
                                    $scope.champions = response.data;
                                    $scope.version = response.version;
                                })
                                .error(function(response){
                                    alert('Une erreur a eu lieu :/');
                                }
                );
            }
            
            
            
       /*******************************************************/
       
       $scope.optionsChart = {
                //animation:false
            };

            // ==========================================
            $scope.position = {
                labels: [
                    "TOP",
                    "JUNGLE",
                    "MID",
                    "ADC",
                    "SUPPORT"
                ],
                data: [
                    30,
                    50,
                    0,
                    0,
                    200
                ]
            };
            
            // ===========================================

        
        
            $scope.stats_champion = {
                labels : ['janv', 'fév.', 'mars', 'mai', 'juin', 'juil.', 'août', 'sept.', 'oct.', 'nov.', 'déc.'],
                series : ['DIAMANT', 'PLATINE', 'OR', 'ARGENT', 'BRONZE'],
                win: [
                    [12, 15, 11, 13, 18, 20], // 
                    [22, 25, 21, 23, 28, 30],  //
                    [8, 4, 5, 9, 10, 3],  // 
                    [2, 3, 6, 0, 2, 3],  //
                    [28, 22, 6, 47, 45, 41]  //
                ],
                pickBan: [
                    [15, 15, 11, 13, 9, 4], // 
                    [25, 25, 46, 33, 20, 30],  //
                    [12, 8, 5, 9, 10, 3],  // 
                    [78, 3, 0, 1, 20, 40],  //
                    [28, 5, 6, 89, 12, 50]  //
                ]
                
            };
            
        
        
    }]);
    
})();