(function(){
    
    angular.module('championModule')
           .controller('championsController',
            ['$scope','$http','$location','$localStorage','ChampionService',
    function($scope,$http,$location,$localStorage,ChampionService)
    {
            $scope.champions = [];
            $scope.cacheActif = true;
            
            ChampionService.getChampions().then(function (response)
                {   $localStorage.champions = response.data;
                    $localStorage.version = response.version;
                    $scope.champions = response.data;
                    $scope.version = response.version;

                }, function ()
                {   // Si ko
                      alert('Une erreur a eu lieu :/');
                });
            
    }]);
    
})();