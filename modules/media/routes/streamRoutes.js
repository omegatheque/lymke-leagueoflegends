(function(){

    angular.module('mediaModule').config(['$locationProvider', '$routeProvider',
            function($location, $routeProvider) {
              $routeProvider.
                
                when('/streams', {
                  templateUrl: angular.module('mediaModule').path + 'views/streams.html',
                  controller: 'streamsController',
                  access: { requiredLogin: false }
                }).
                when('/webtv', {
                  templateUrl: angular.module('mediaModule').path + 'views/webtv.html',
                  controller: 'WebtvController',
                  access: { requiredLogin: false }
                }).
                when('/worlds', {
                  templateUrl: angular.module('mediaModule').path + 'views/worlds.html',
                  controller: 'WorldsController',
                  access: { requiredLogin: false }
                }).
                when('/lcs', {
                  templateUrl: angular.module('mediaModule').path + 'views/lcs.html',
                  controller: 'LcsController',
                  access: { requiredLogin: false }
                });

            }]);
})();