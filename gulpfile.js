// Gulp

var gulp = require('gulp');

// Importing Gulp dependencies
var
        uglify = require('gulp-uglify'),
        minifyCSS = require('gulp-minify-css'),
        rename = require('gulp-rename'),
        jshint = require('gulp-jshint'),
        gp_concat = require('gulp-concat'),
        minifyHTML = require('gulp-minify-html'),
        jsonminify = require('gulp-jsonminify'),
        watch = require('gulp-watch');

//Modules and folders

var destination = '../../prod/frontend//js/';
var modules = ['app', 'layout', 'champion', 'summoner', 'media', 'user', 'tool'];


modules.forEach(function (module) {

    //JAVASCRIPT

    var js_sources = [
        "modules/" + module + "/*.js",
        "modules/" + module + "/config/*.js",
        "modules/" + module + "/controllers/*.js",
        "modules/" + module + "/directives/*.js",
        "modules/" + module + "/routes/*.js",
        "modules/" + module + "/run/*.js",
        "modules/" + module + "/services/*.js"
    ]
            ;

    gulp.task(module, function () {

        return gulp.src(js_sources)
                .pipe(jshint())
                .pipe(gp_concat(module + '.js'))
                .pipe(uglify({mangle: false}))
                .pipe(rename({suffix: '.min'}))
                .pipe(gulp.dest(destination));
        //.pipe(watch(js_sources)); 

    });
});


// CSS
/*gulp.task('css', function() {
 
 return gulp
 .src(tasks.css.source)
 .pipe(minifyCSS({
 keepSpecialComments: 1
 }))
 .pipe(rename({
 suffix: '.min'
 }))
 .pipe(gulp.dest(tasks.css.dest));
 
 });
 */


// Default tasks (called when running `gulp` from cli)

gulp.task('default', modules);